﻿using System.ComponentModel;
using System.Windows;
using Uno.GameClient.Wpf.ViewModels;

namespace Uno.GameClient.Wpf.Views
{

    /// <summary>
    /// Interaction logic for GameBoardWindow.xaml
    /// </summary>
    public partial class GameBoardWindow : Window
    {
        public GameBoardWindow()
        {
            InitializeComponent();
        }

        private void GameBoardWindow_Closing(object sender, CancelEventArgs e)
        {
            if (DataContext is IClosing context)
            {
                context.OnClosing();
            }
        }
    }

}
