﻿using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.Rooms;
using Uno.GameClient.Wpf.Services;

namespace Uno.GameClient.Wpf.ViewModels
{
    internal class LobbyWindowViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;

        private readonly UserData? _user;

        public string? UserNickName { get => _user?.NickName; }

        // список всех игровых комнат
        public ObservableCollection<RoomData> Rooms { get; private set; } = new();

        public string NewRoomName { get; set; } = string.Empty;

        private int _maxPlayersCount = 4;
        public int MaxPlayersCount
        {
            get { return _maxPlayersCount; }
            set
            {
                _maxPlayersCount = value;
                OnPropertyChanged(nameof(MaxPlayersCount));
            }
        }

        internal record MessageData(string User, string Message);

        // список всех полученных сообщений
        internal ObservableCollection<MessageData> Messages { get; } = new();

        private RoomData? _selectedRoom;

        public RoomData? SelectedRoom
        {
            get => _selectedRoom;

            set
            {
                if (_selectedRoom != value)
                {
                    _selectedRoom = value;
                    OnPropertyChanged(nameof(SelectedRoom));
                }
            }
        }

        // команда отправки сообщений
        private ICommand? _sendMessageCommand;
        public ICommand SendMessageCommand => _sendMessageCommand ??= new RelayCommand<string>(async (cmd) => await SendSignalrMessage(cmd!), _ => IsConnected && !IsBusy);

        // осуществлено ли подключение
        bool isConnected;
        public bool IsConnected
        {
            get => isConnected;
            set
            {
                if (isConnected != value)
                {
                    isConnected = value;
                    OnPropertyChanged(nameof(IsConnected));
                }
            }
        }

        // идет ли отправка сообщений
        bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy != value)
                {
                    isBusy = value;
                    OnPropertyChanged(nameof(IsBusy));
                }
            }
        }

        public LobbyWindowViewModel(INavigationService navigationService, UserData? user)
        {
            _navigationService = navigationService;
            _user = user;

            IsConnected = false;    // по умолчанию не подключены
            IsBusy = false;         // отправка сообщения не идет

            _navigationService.SignalrHubConnection!.Closed += async (error) =>
            {
                SendLocalMessage(string.Empty, "Подключение закрыто...");
                IsConnected = false;
                await Task.Delay(5000);
                await Connect(); // попытка переподключения через 5 секунд
            };

            var oc = SynchronizationContext.Current;

            _navigationService.SignalrHubConnection!.On<string>("ReceiveErrorMessage", (error) =>
            {
                oc!.Post((_) =>
                {
                    navigationService.ShowErrorMessage(error);
                }, null);
            });

            _navigationService.SignalrHubConnection!.On<RoomData>("ReceiveRoom", (room) =>
            {
                if (_user != null)
                {
                    oc!.Post((_) =>
                    {
                        _navigationService.ShowGameBoardWindow(room, _user);
                    }, null);
                }
            });

            _navigationService.SignalrHubConnection!.On<IList<RoomData>>("ReceiveRooms", (rooms) =>
            {
                oc!.Post((_) =>
                {
                    RoomData? previousSelectedRoom = SelectedRoom;
                    Rooms.Clear();
                    foreach (var room in rooms)
                    {
                        Rooms.Add(room);
                    }
                    if (previousSelectedRoom == null)
                    {
                        if (Rooms.Count > 0)
                            SelectedRoom = Rooms[0];
                    }
                    else
                    {
                        var matchedPreviousRoom = Rooms.Where(_ => _?.Id == previousSelectedRoom?.Id).FirstOrDefault();
                        if (matchedPreviousRoom != null)
                            SelectedRoom = matchedPreviousRoom;
                    }
                }, null);
            });
        }

        //// Финализатор
        //~AuthWindowViewModel()
        //{
        //    Disconnect();
        //}

        // подключение к чату
        public async Task Connect()
        {
            if (IsConnected)
                return;
            try
            {
                await _navigationService.SignalrHubConnection!.StartAsync();
                SendLocalMessage(string.Empty, "Вы вошли в комнату...");
                IsConnected = true;
            }
            catch (Exception ex)
            {
                SendLocalMessage(string.Empty, $"Ошибка подключения: {ex.Message}");
            }
            if (IsConnected)
            {
                await SendSignalrMessage(SignalrMessage.GetRooms);
            }
        }

        // Отправка сообщения
        async Task SendSignalrMessage(string remoteSignalrTaskName)
        {
            try
            {
                IsBusy = true;
                switch (remoteSignalrTaskName)
                {
                    case SignalrMessage.JoinToRoom:
                        if (SelectedRoom != null)
                        {
                            var roomPlayer = new RoomPlayerData()
                            {
                                PlayerId = _user!.Id,
                                Status = RoomPlayerData.ConnectionStatus.Connected,
                                UserName = _user.NickName
                            };
                            await _navigationService.SignalrHubConnection!.InvokeAsync(remoteSignalrTaskName, SelectedRoom, roomPlayer);
                        }
                        break;
                    case SignalrMessage.CreateRoom:
                        var roomCreator = new RoomPlayerData()
                        {
                            PlayerId = _user!.Id,
                            Status = RoomPlayerData.ConnectionStatus.Connected,
                            UserName = _user.NickName
                        };
                        await _navigationService.SignalrHubConnection!.InvokeAsync(remoteSignalrTaskName, NewRoomName, MaxPlayersCount, roomCreator)/*.ConfigureAwait(false)*/;
                        break;
                    // Получение активных комнат
                    case SignalrMessage.GetRooms:
                        await _navigationService.SignalrHubConnection!.InvokeAsync(remoteSignalrTaskName);
                        break;

                }
            }
            catch (Exception ex)
            {
                //_navigationService.ShowErrorMessage(ex.Message);
                SendLocalMessage(string.Empty, $"Ошибка отправки: {ex.Message}");
            }

            IsBusy = false;
        }

        // Добавление сообщения
        private void SendLocalMessage(string user, string message)
        {
            Messages.Insert(0, new MessageData(user, message));
        }
    }
}
