﻿using System.Windows.Media;

namespace Uno.GameClient.Wpf.ViewModels
{
    class CardViewModel : BaseViewModel
    {
        public Guid Id { get; set; }
        public ImageSource? CardImage { get; set; }
        public double CoordX { get; set; }
        public double CoordY { get; set; }
        public int RotationAngle { get; set; }
    }
}
