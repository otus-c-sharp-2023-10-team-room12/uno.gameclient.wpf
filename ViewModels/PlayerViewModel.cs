﻿using System.Collections.ObjectModel;
using Uno.CommonLibrary.Auth;

namespace Uno.GameClient.Wpf.ViewModels
{
    internal class PlayerViewModel : BaseViewModel
    {
        public UserData? User { get; set; }

        private bool _isPlayerAvatarBlinking = false;

        public bool IsPlayerAvatarBlinking
        {
            get { return _isPlayerAvatarBlinking; }
            set
            {
                _isPlayerAvatarBlinking = value;
                OnPropertyChanged(nameof(IsPlayerAvatarBlinking));
            }
        }

        public ObservableCollection<CardViewModel> PlayerHandCardsList { get; set; } = new ObservableCollection<CardViewModel>();
        
    }
}
