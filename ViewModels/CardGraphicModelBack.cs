﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace Uno.GameClient.Wpf.ViewModels
{
    /// <summary>
    /// Рубашка карты
    /// </summary>
    class CardGraphicModelBack : CardGraphicModel
    {
        public CardGraphicModelBack()
        {
            cardImage = new CroppedBitmap(allCardsImage as BitmapSource, new Int32Rect(0, 1440, cardImageWidth, cardImageHeight));
        }
    }
}
