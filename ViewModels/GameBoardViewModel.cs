﻿using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.GamePlay;
using Uno.CommonLibrary.Rooms;
using Uno.GameClient.Wpf.Models;
using Uno.GameClient.Wpf.Services;

namespace Uno.GameClient.Wpf.ViewModels
{
    internal class GameBoardViewModel : BaseViewModel, IClosing
    {
        private int lastAngleForTopCardOfPlayedDeck = 0;

        internal record MessageData(string User, string Message);

        public Visibility LeftDockPanelVisibility
        {
            get => _room.TotalPlayers > 2 ? Visibility.Visible : Visibility.Collapsed;
        }

        public Visibility RightDockPanelVisibility
        {
            get => _room.TotalPlayers > 3 ? Visibility.Visible : Visibility.Collapsed;
        }

        private GameState _currentGameState;

        /// <summary>
        /// Текущее состояние игры
        /// </summary>
        public GameState CurrentGameState
        {
            get => _currentGameState;

            set
            {
                if (_currentGameState != value)
                {
                    _currentGameState = value;
                    OnPropertyChanged(nameof(CurrentGameState));
                }
                switch (CurrentGameState)
                {
                    case GameState.WaitingForAllPlayersConnection:
                        CurrentGameStateDescription = "ожидание подключения игроков...";
                        break;
                    case GameState.WaitingForStarting:
                        CurrentGameStateDescription = "ожидание начала игры...";
                        break;
                    case GameState.WaitingForCurrentPlayerAction:
                        if (CurrentPlayerUser != null)
                            CurrentGameStateDescription = CurrentPlayerUser.Id == ClientPlayer?.User?.Id ? "ваш ход..." : $"ожидание хода игрока {CurrentPlayerUser.NickName}...";
                        break;
                    case GameState.Started:
                        CurrentGameStateDescription = "игра начата";
                        break;
                    case GameState.Ended:
                        CurrentGameStateDescription = "игра закончена";
                        if (WinnerPlayerUser != null)
                            CurrentGameStateDescription += ", " +  (WinnerPlayerUser.Id == ClientPlayer?.User?.Id ? "вы победили!" : $"победитель - {WinnerPlayerUser.NickName}");
                        break;
                }
            }
        }

        private string _currentGameStateDescription = string.Empty;

        /// <summary>
        /// Описание текущего состояния игры
        /// </summary>
        public string CurrentGameStateDescription
        {
            get => _currentGameStateDescription;

            set
            {
                if (_currentGameStateDescription != value)
                {
                    _currentGameStateDescription = value;
                    OnPropertyChanged(nameof(CurrentGameStateDescription));
                }
            }
        }

        // список всех полученных сообщений
        internal ObservableCollection<MessageData> Messages { get; } = new();

        private readonly CardGraphicModelsFactory cardGraphicModelsFactory = new();

        private readonly INavigationService _navigationService;

        private readonly RoomData _room;

        public string RoomName { get => _room.Name ?? string.Empty; }

        public PlayerViewModel? _clientPlayer;

        public PlayerViewModel? ClientPlayer
        {
            get { return _clientPlayer; }
            set
            {
                _clientPlayer = value;
                OnPropertyChanged(nameof(ClientPlayer));
            }
        }

        private PlayerViewModel? _leftPlayer;

        public PlayerViewModel? LeftPlayer
        {
            get { return _leftPlayer; }
            set
            {
                _leftPlayer = value;
                OnPropertyChanged(nameof(LeftPlayer));
            }
        }

        private PlayerViewModel? _topPlayer;

        public PlayerViewModel? TopPlayer
        {
            get { return _topPlayer; }
            set
            {
                _topPlayer = value;
                OnPropertyChanged(nameof(TopPlayer));
            }
        }

        private PlayerViewModel? _rightPlayer;

        public PlayerViewModel? RightPlayer
        {
            get { return _rightPlayer; }
            set
            {
                _rightPlayer = value;
                OnPropertyChanged(nameof(RightPlayer));
            }
        }

        private int _playersCount = 1;

        public int PlayersCount
        {
            get { return _playersCount; }
            set
            {
                if (_playersCount != value)
                {
                    _playersCount = value;
                    if (CurrentGameState == GameState.WaitingForAllPlayersConnection)
                    {
                        int RemainingPlayersCount = _room == null ? 0 : _room.TotalPlayers - PlayersCount;
                        CurrentGameStateDescription = $"ожидание подключения{(RemainingPlayersCount == 0 ? string.Empty : $" ещё {RemainingPlayersCount}")} игрок{(RemainingPlayersCount == 1 ? "а" : "ов")}...";
                    }
                    LeftPlayer = TopPlayer = RightPlayer = null;
                    if (PlayersCount > 1)
                    {
                        if (PlayersCount == 2)
                        {
                            if (ClientPlayer?.User != null)
                            {
                                if (_room?.TotalPlayers == 2)
                                {
                                    TopPlayer = GetNextPlayerAfterTarget(ClientPlayer.User.Id);
                                }
                                else
                                {
                                    LeftPlayer = GetNextPlayerAfterTarget(ClientPlayer.User.Id);
                                }
                            }
                        }
                        else if (PlayersCount > 2)
                        {
                            if (ClientPlayer?.User != null)
                                LeftPlayer = GetNextPlayerAfterTarget(ClientPlayer.User.Id);
                            if (LeftPlayer?.User != null)
                                TopPlayer = GetNextPlayerAfterTarget(LeftPlayer.User.Id);
                            if (PlayersCount == 4 && TopPlayer?.User != null)
                                RightPlayer = GetNextPlayerAfterTarget(TopPlayer.User.Id);
                        }
                    }
                    if (PlayersCount == _room?.TotalPlayers && (CurrentGameState == GameState.WaitingForAllPlayersConnection || CurrentGameState == GameState.Ended))
                        CurrentGameState = GameState.WaitingForStarting;
                    OnPropertyChanged(nameof(PlayersCount));
                }
            }
        }

        // список всех остальных игроков в комнате
        public ObservableCollection<PlayerViewModel> OtherPlayers { get; private set; } = new();

        // список всех игроков в комнате
        public ObservableCollection<PlayerViewModel> RoomPlayers { get; private set; } = new();

        /// <summary>
        /// Колода прикупа
        /// </summary>
        public ObservableCollection<CardViewModel> RemainingDeckCardsList { get; set; } = new ObservableCollection<CardViewModel>();

        /// <summary>
        /// Колода сброса 
        /// </summary>
        public ObservableCollection<CardViewModel> PlayedDeckCardsList { get; set; } = new ObservableCollection<CardViewModel>();

        private UserData? _currentPlayerUser;

        /// <summary>
        /// Игрок, осуществляющий в данный момент ход
        /// </summary>
        public UserData? CurrentPlayerUser
        {
            get => _currentPlayerUser;

            set
            {
                if (_currentPlayerUser != value)
                {
                    _currentPlayerUser = value;
                    OnPropertyChanged(nameof(CurrentPlayerUser));
                }
            }
        }

        private UserData? _winnerPlayerUser;

        /// <summary>
        /// Игрок, победивший в игре
        /// </summary>
        public UserData? WinnerPlayerUser
        {
            get => _winnerPlayerUser;

            set
            {
                if (_winnerPlayerUser != value)
                {
                    _winnerPlayerUser = value;
                    OnPropertyChanged(nameof(WinnerPlayerUser));
                }
            }
        }


        public GameBoardViewModel(INavigationService navigationService, RoomData room, UserData user)
        {
            CurrentGameState = GameState.WaitingForAllPlayersConnection;
            _navigationService = navigationService;
            _room = room;
            ClientPlayer = new PlayerViewModel() { User = user, PlayerHandCardsList = new ObservableCollection<CardViewModel>(), IsPlayerAvatarBlinking = false };

            IsConnected = false;    // по умолчанию не подключены
            IsBusy = false;         // отправка сообщения не идет
            RoomPlayers.CollectionChanged += (sender, e) =>
            {
                PlayersCount = RoomPlayers.Count;
            };

            _navigationService.SignalrHubConnection!.Closed += async (error) =>
            {
                SendLocalMessage(string.Empty, "Подключение закрыто...");
                IsConnected = false;
                await Task.Delay(5000);
                await Connect(); // попытка переподключения через 5 секунд
            };
            var oc = SynchronizationContext.Current;

            _navigationService.SignalrHubConnection!.On<IEnumerable<UserData>>("ReceiveRoomPlayers", (players) =>
            {
                oc!.Post((_) =>
                {
                    RoomPlayers.Clear();
                    OtherPlayers.Clear();
                    foreach (var player in players)
                    {
                        var roomPlayer = new PlayerViewModel() { IsPlayerAvatarBlinking = false, User = player, PlayerHandCardsList = new ObservableCollection<CardViewModel>() };
                        RoomPlayers.Add(roomPlayer);
                        if (player.Id != ClientPlayer?.User?.Id)
                            OtherPlayers.Add(roomPlayer);
                    }
                    if (CurrentGameState == GameState.Undefined)
                        CurrentGameState = GameState.WaitingForAllPlayersConnection;
                }, null);
            });

            _navigationService.SignalrHubConnection!.On<CardData, bool>("ReceiveTopCardAfterDeckGeneration", (card, isClientPlayerInvoker) =>
            {
                oc!.Post(async (_) =>
                {
                    await StartGameAfterDeckGeneration(card, isClientPlayerInvoker);
                }, null);
            });

            _navigationService.SignalrHubConnection!.On<CardData>("ReceiveLastPlayedCard", (card) =>
            {
                oc!.Post((_) =>
                {
                    PutTopCardOnPlayedDeck(card);
                }, null);
            });

            _navigationService.SignalrHubConnection!.On<UserData, int>("UpdateGameState", (targetPlayer, gameStateEnumNumber) =>
            {
                oc!.Post((_) =>
                {
                    if (typeof(GameState).IsEnumDefined(gameStateEnumNumber))
                    {
                        GameState newGameState = (GameState)gameStateEnumNumber;
                        switch (newGameState)
                        {
                            case GameState.WaitingForCurrentPlayerAction:
                                CurrentPlayerUser = targetPlayer;
                                if (ClientPlayer != null)
                                {
                                    ClientPlayer.IsPlayerAvatarBlinking = ClientPlayer.User.Id == targetPlayer.Id;
                                }
                                for (int i = 0; i < OtherPlayers.Count; i++)
                                {
                                    OtherPlayers[i].IsPlayerAvatarBlinking = OtherPlayers[i].User?.Id == targetPlayer.Id;
                                }
                                break;
                            case GameState.Ended:
                                if (ClientPlayer != null)
                                {
                                    ClientPlayer.IsPlayerAvatarBlinking = false;
                                }
                                for (int i = 0; i < OtherPlayers.Count; i++)
                                {
                                    OtherPlayers[i].IsPlayerAvatarBlinking = false;
                                }
                                CurrentPlayerUser = null;
                                WinnerPlayerUser = targetPlayer;
                                break;
                            default:
                                break;
                        }
                        CurrentGameState = newGameState;
                    }
                }, null);
            });

            _navigationService.SignalrHubConnection!.On<Guid, IList<CardData>>("UpdatePlayerHandCards", (targetPlayerId, cards) =>
            {
                bool targetPlayerIsOtherPlayer = false;
                // Обновление карт игрока для требуемого клиента:
                for (int i = 0; i < OtherPlayers.Count; i++)
                {
                    var player = OtherPlayers[i];
                    if (player.User != null && player.User.Id == targetPlayerId)
                    {
                        targetPlayerIsOtherPlayer = true;
                        bool IsOtherPlayerLeftPlayer = LeftPlayer != null && LeftPlayer.User?.Id == player.User.Id,
                             IsOtherPlayerTopPlayer = !IsOtherPlayerLeftPlayer && TopPlayer != null && TopPlayer.User?.Id == player.User.Id,
                             IsOtherPlayerRightPlayer = !IsOtherPlayerLeftPlayer && !IsOtherPlayerTopPlayer && RightPlayer != null && RightPlayer.User?.Id == player.User.Id;
                        int playerCanvasCoordX = IsOtherPlayerRightPlayer ? 90 : 0,
                            playerCanvasCoordY = IsOtherPlayerRightPlayer ? 10 : (IsOtherPlayerLeftPlayer && _room.TotalPlayers > 2 ? 70 : 0);
                        oc!.Post((_) =>
                        {
                            CardGraphicModel cardGraphicModelBack = cardGraphicModelsFactory.GetCardGraphicModelBack();
                            player.PlayerHandCardsList.Clear();
                            for (int j = 0; j < cards.Count; j++)
                            {
                                cardGraphicModelBack.Draw(player.PlayerHandCardsList, playerCanvasCoordX, playerCanvasCoordY, cards[j].Id, IsOtherPlayerLeftPlayer && _room.TotalPlayers > 2 ? -90 : (IsOtherPlayerRightPlayer ? 90 : 0));
                                if (IsOtherPlayerTopPlayer || _room.TotalPlayers == 2)
                                    playerCanvasCoordX += 40;
                                else
                                    playerCanvasCoordY += 36;
                            }
                        }, null);
                        break;
                    }
                }
                if (!targetPlayerIsOtherPlayer && ClientPlayer != null && ClientPlayer.User.Id == targetPlayerId)
                {
                    oc!.Post((_) =>
                    {
                        // Обновление карт игрока для клиента:
                        ClientPlayer.PlayerHandCardsList.Clear();
                        int clientPlayerCanvasCoordX = 0,
                            clientPlayerCanvasCoordY = 0;
                        foreach (CardData card in cards)
                        {
                            CardColour cardColour = card.Color.GetCardColourFromGamePlayColourNumber();
                            CardGraphicModel cardGraphicModelFront = cardGraphicModelsFactory.GetCardGraphicModelFront(card.Value, cardColour);
                            cardGraphicModelFront.Draw(ClientPlayer.PlayerHandCardsList, clientPlayerCanvasCoordX, clientPlayerCanvasCoordY, card.Id);
                            clientPlayerCanvasCoordX += 40;
                        }
                    }, null);
                }
            });

            _navigationService.SignalrHubConnection!.On("SwitchToNextPlayer", () =>
            {
                oc!.Post(async (_) =>
                {
                    if (CurrentPlayerUser != null)
                    {
                        CurrentPlayerUser = GetNextPlayerAfterTarget(CurrentPlayerUser.Id)?.User;
                        CurrentGameState = GameState.WaitingForCurrentPlayerAction;
                        await SendSignalrMessage(SignalrMessage.ChangeGameState);
                    }
                }, null);
            });

            _navigationService.SignalrHubConnection!.On("WinGame", () =>
            {
                oc!.Post(async (_) =>
                {
                    CurrentGameState = GameState.Ended;
                    WinnerPlayerUser = ClientPlayer.User;
                    await SendSignalrMessage(SignalrMessage.ChangeGameState);
                }, null);
            });

        }

        /// <summary>
        /// Начать новую игру после завершения генерации колоды и раздачи карт игрокам
        /// </summary>
        /// <returns></returns>
        public async Task StartGameAfterDeckGeneration(CardData topCard, bool isClientPlayerInvoker)
        {
            CurrentGameState = GameState.Started;
            await SendSignalrMessage(SignalrMessage.ChangeGameState);
            lastAngleForTopCardOfPlayedDeck = 0;
            CardGraphicModel cardGraphicModelBack = cardGraphicModelsFactory.GetCardGraphicModelBack();
            RemainingDeckCardsList.Clear();
            // Обложка верхней карты для колоды прикупа
            cardGraphicModelBack.Draw(RemainingDeckCardsList, 0, 0, Guid.Empty, 0);

            // Верхняя карта для колоды сброса
            PlayedDeckCardsList.Clear();
            PutTopCardOnPlayedDeck(topCard);

            await SendSignalrMessage(SignalrMessage.GetPlayerCards);
            if (isClientPlayerInvoker)
            {
                if (ClientPlayer != null)
                    ClientPlayer.IsPlayerAvatarBlinking = true;
                for (int i = 0; i < OtherPlayers.Count; i++)
                {
                    OtherPlayers[i].IsPlayerAvatarBlinking = false;
                }

                WinnerPlayerUser = null;
                CurrentPlayerUser = ClientPlayer?.User;
                CurrentGameState = GameState.WaitingForCurrentPlayerAction;
                await SendSignalrMessage(SignalrMessage.ChangeGameState);
            }
        }

        /// <summary>
        /// Обновить верхнюю карту в колоде сброса
        /// </summary>
        /// <returns></returns>
        public void PutTopCardOnPlayedDeck(CardData topCard)
        {
            // Верхняя карта для колоды сброса
            CardGraphicModel topCardGraphicModelFront = cardGraphicModelsFactory.GetCardGraphicModelFront(topCard.Value, topCard.Color.GetCardColourFromGamePlayColourNumber());
            int angle = 0;
            if (PlayedDeckCardsList.Count > 0)
            {
                do
                {
                    angle = new Random().Next(lastAngleForTopCardOfPlayedDeck > 0 ? -30 : 5, lastAngleForTopCardOfPlayedDeck > 0 ? -5 : 30);
                }
                while (angle == lastAngleForTopCardOfPlayedDeck);
            };
            lastAngleForTopCardOfPlayedDeck = angle;
            topCardGraphicModelFront.Draw(PlayedDeckCardsList, 0, 0, topCard.Id, angle);
        }

        // идет ли отправка сообщений
        bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy != value)
                {
                    isBusy = value;
                    OnPropertyChanged(nameof(IsBusy));
                }
            }
        }

        // осуществлено ли подключение
        bool isConnected;
        public bool IsConnected
        {
            get => isConnected;
            set
            {
                if (isConnected != value)
                {
                    isConnected = value;
                    OnPropertyChanged(nameof(IsConnected));
                }
            }
        }

        private PlayerViewModel? GetNextPlayerAfterTarget(Guid targetPlayerUserId)
        {
            PlayerViewModel? nextPlayer = null;
            for (int i = 0; i < RoomPlayers.Count; i++)
            {
                if (RoomPlayers[i]?.User?.Id == targetPlayerUserId)
                {
                    nextPlayer = RoomPlayers[RoomPlayers.Count > i + 1 ? i + 1 : 0];
                    break;
                }
            }
            return nextPlayer;
        }

        // подключение к чату
        public async Task Connect()
        {
            if (IsConnected)
                return;
            else if (_navigationService.SignalrHubConnection!.State == HubConnectionState.Connected)
            {
                IsConnected = true;
            }
            else
            {
                try
                {
                    await _navigationService.SignalrHubConnection!.StartAsync();
                    SendLocalMessage(string.Empty, "Вы вошли в игру...");
                    IsConnected = true;
                }
                catch (Exception ex)
                {
                    SendLocalMessage(string.Empty, $"Ошибка подключения: {ex.Message}");
                }
            }
            if (IsConnected)
            {
                await SendSignalrMessage(SignalrMessage.GetRoomPlayers);
            }
        }

        private ICommand? _startGameCommand;
        public ICommand StartGameCommand => _startGameCommand ??= new RelayCommand(async (_) =>
            {
                await SendSignalrMessage(SignalrMessage.GenerateDeck);
            }, _ => IsConnected && (CurrentGameState == GameState.Undefined || CurrentGameState == GameState.Ended || CurrentGameState == GameState.WaitingForStarting) && _room.TotalPlayers == PlayersCount && !IsBusy);

        private ICommand? _clientPlayerCardMoveCommand;
        public ICommand ClientPlayerCardMoveCommand => _clientPlayerCardMoveCommand ??= new RelayCommand<Guid>(async (guid) =>
        {
            if (!IsBusy && IsConnected && CurrentGameState == GameState.WaitingForCurrentPlayerAction && ClientPlayer?.User != null && CurrentPlayerUser?.Id == ClientPlayer?.User.Id && _navigationService.SignalrHubConnection?.State == HubConnectionState.Connected)
            {
                try
                {
                    IsBusy = true;
                    if (guid == Guid.Empty)
                    {
                        await _navigationService.SignalrHubConnection!.InvokeAsync(SignalrMessage.GetDeckCard, _room, ClientPlayer?.User.Id, 1);
                    }
                    else
                    {
                        await _navigationService.SignalrHubConnection!.InvokeAsync(SignalrMessage.PlayCard, _room, ClientPlayer?.User.Id, guid);
                    }
                    await SendSignalrMessage(SignalrMessage.GetPlayerCards);
                }
                catch (Exception ex)
                {
                    _navigationService.ShowErrorMessage(ex.Message);
                }
                IsBusy = false;
            }
        }, _ => true);

        // Отправка сообщения
        async Task SendSignalrMessage(string signalrTaskName)
        {
            if (IsConnected && _navigationService.SignalrHubConnection?.State == HubConnectionState.Connected)
            {
                try
                {
                    IsBusy = true;
                    switch (signalrTaskName)
                    {
                        case SignalrMessage.DisconnectFromRoom:
                            if (ClientPlayer?.User != null)
                            {
                                await _navigationService.SignalrHubConnection!.InvokeAsync(signalrTaskName, _room, ClientPlayer.User.Id);
                            }
                            break;
                        case SignalrMessage.GenerateDeck:
                        case SignalrMessage.GetRoomPlayers:
                            await _navigationService.SignalrHubConnection!.InvokeAsync(signalrTaskName, _room);
                            break;
                        case SignalrMessage.GetPlayerCards:
                            
                            if (ClientPlayer?.User != null)
                                await _navigationService.SignalrHubConnection!.InvokeAsync(signalrTaskName, _room, ClientPlayer.User.Id);
                            for (int i = 0; i < OtherPlayers.Count; i++)
                            {
                                var player = OtherPlayers[i];
                                if (player.User != null)
                                    await _navigationService.SignalrHubConnection!.InvokeAsync(signalrTaskName, _room, player.User.Id);
                            }
                            break;
                        case SignalrMessage.ChangeGameState:
                            var TargetPlayerUser = CurrentGameState == GameState.Ended ? WinnerPlayerUser : CurrentPlayerUser;
                            await _navigationService.SignalrHubConnection!.InvokeAsync(signalrTaskName, _room, TargetPlayerUser, (int)CurrentGameState);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    _navigationService.ShowErrorMessage(ex.Message);
                }
            }
            IsBusy = false;
        }

        public async void OnClosing()
        {
            await SendSignalrMessage(SignalrMessage.DisconnectFromRoom);
        }

        // Добавление сообщения
        private void SendLocalMessage(string user, string message)
        {
            Messages.Insert(0, new MessageData(user, message));
        }

    }
}
