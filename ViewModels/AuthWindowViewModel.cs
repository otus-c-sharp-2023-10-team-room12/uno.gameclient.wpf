﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Windows.Controls;
using System.Windows.Input;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.Common;
using Uno.GameClient.Wpf.Services;

namespace Uno.GameClient.Wpf.ViewModels
{
    internal class AuthWindowViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;

        private readonly string authServiceUrl = ServicesDictionary.GetServiceAddress("Auth", false);
        
        public string Email { get; set; } = string.Empty;

        private string _userPassword = string.Empty;

        public string UserPassword { get => _userPassword; }

        private ICommand? _passwordChangedCommand;
        public ICommand PasswordChangedCommand => _passwordChangedCommand ??= new RelayCommand<PasswordBox>(_ => _userPassword = _!.Password, _ => true);

        // команда авторизации пользователя
        private ICommand? _authUserCommand;
        public ICommand AuthUserCommand => _authUserCommand ??= new RelayCommand(async _ => await AuthUser(), _ => true);


        // Отправка сообщения
        async Task AuthUser()
        {
            using (var httpClient = new HttpClient())
            {
                var loginUserRequest = new LoginUserRequest() { Email = Email, Password = UserPassword };
                using (var response = await httpClient.PostAsJsonAsync($"{authServiceUrl}/api/User/auth", loginUserRequest))
                {
                    if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        string? error = await response.Content.ReadAsStringAsync();
                        _navigationService.ShowErrorMessage(error);
                    }
                    else if (response.StatusCode == HttpStatusCode.OK)
                    {
                        // считываем ответ
                        UserData? user = await response.Content.ReadFromJsonAsync<UserData>();
                        _navigationService.UpdateMutex($"AuthWindow{user!.Id}");
                        if (_navigationService.CheckForMutex())
                        {
                            _navigationService.ShowLobbyWindow(user);
                        }
                        else
                        {
                            _navigationService.CloseCurrentWindow(); // В памяти уже есть экземпляр с данным пользователем
                            _navigationService.ShowErrorMessage("Для данного пользователя экземпляр игры уже запущен.");
                        }
                    }
                }
            }
        }

        public AuthWindowViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
    }
}
