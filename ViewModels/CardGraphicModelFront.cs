﻿using System.Windows;
using System.Windows.Media.Imaging;
using Uno.GameClient.Wpf.Models;

namespace Uno.GameClient.Wpf.ViewModels
{
    /// <summary>
    /// Лицевая часть карты
    /// </summary>
    class CardGraphicModelFront : CardGraphicModel
    {
        /// <summary>
        /// Значение карты
        /// </summary>
        protected int _value;

        /// <summary>
        /// Цвет карты
        /// </summary>
        protected CardColour _colour;

        public CardGraphicModelFront(int value, CardColour colour)
        {
            _value = value;
            _colour = colour;
            int xCoordFromAllCardsImage = _value * 240,
                yCoordFromAllCardsImage = (int)_colour * 360;
            cardImage = new CroppedBitmap(allCardsImage as BitmapSource, new Int32Rect(xCoordFromAllCardsImage, yCoordFromAllCardsImage, cardImageWidth, cardImageHeight));
        }
    }

}
