﻿using System.Windows.Input;

namespace Uno.GameClient.Wpf.ViewModels
{
    internal class RelayCommand<T> : ICommand
    {
        private readonly Action<T?> _action;
        private readonly Predicate<T?>? _canExecute;

        /// <summary>
        /// Инициализация нового экземпляра класса с параметрами <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="action">Параметризированное действие.</param>
        /// <param name="canExecute"> Если установлено в <c>true</c> [can execute](выполнение разрешено).</param>
        public RelayCommand(Action<T?> action, Predicate<T?>? canExecute)
        {
            //  Set the action.
            _action = action;
            _canExecute = canExecute;
        }

        public RelayCommand(Action<T?> action)
            : this(action, null)
        {
        }

        public bool CanExecute(object? parameter)
        {
            return _canExecute == null || _canExecute((T?)parameter);
        }

        public event EventHandler? CanExecuteChanged
        {
            add { }
            remove { }
            //add { CommandManager.RequerySuggested += value; }
            //remove { CommandManager.RequerySuggested -= value; }

        }

        public void Execute(object? parameter)
        {
            _action?.Invoke((T?)parameter);
        }
    }

    internal class RelayCommand : ICommand
    {
        private readonly Action<object?>? _action;
        private readonly Predicate<object?>? _canExecute;

        public RelayCommand(Action<object?>? action, Predicate<object?>? canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public RelayCommand(Action<object?> action)
           : this(action, null)
        {
            _action = action;
        }

        public bool CanExecute(object? parameter)
        {
            return _canExecute == null || _canExecute(parameter);
        }

        public event EventHandler? CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
        public void Execute(object? parameter)
        {
            _action?.Invoke(parameter);
        }
    }
}
