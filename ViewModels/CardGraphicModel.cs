﻿using System.Windows.Media;
using Uno.GameClient.Wpf.Services;

namespace Uno.GameClient.Wpf.ViewModels
{
    abstract class CardGraphicModel
    {
        protected static readonly ImageSource? allCardsImage = ImageServices.RetrieveImageSourceFromUri("pack://application:,,,/Resources/Cards.png");

        /// <summary>
        /// Ширина изображения карты
        /// </summary>
        protected const int cardImageWidth = 242;

        /// <summary>
        /// Высота изображения карты
        /// </summary>
        protected const int cardImageHeight = 362;

        /// <summary>
        /// Изображение карты
        /// </summary>
        protected ImageSource? cardImage;

        /// <summary>
        /// Метод отрисовки карты
        /// </summary>
        /// <param name="playerHandCardsList">"Рука" игрока с коллекцией карт</param>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="angle">Угол поворота</param>
        /// <param name="id">Id карты в колоде</param>
        public void Draw(IList<CardViewModel> playerHandCardsList, double x, double y, Guid id, int angle = 0)
        {
            if (cardImage != null) 
            {
                var cardviewModel = new CardViewModel { CardImage = cardImage, CoordX = x, CoordY = y, Id = id, RotationAngle = angle };
                playerHandCardsList.Add(cardviewModel);
            }
        }
    }
}
