﻿using Microsoft.AspNetCore.SignalR.Client;
using System.Windows;
using Uno.GameClient.Wpf.Services;

namespace Uno.GameClient.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        INavigationService? navigationService;
        protected override /*async*/ void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            navigationService = new NavigationService();
            navigationService.ShowAuthWindow();
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            if (navigationService!.SignalrHubConnection != null && navigationService!.SignalrHubConnection.State == HubConnectionState.Connected)
                await navigationService!.SignalrHubConnection!.StopAsync();
            navigationService?.CloseMutex();
            base.OnExit(e);
        }
    }
}
