﻿using System.Windows.Media;

namespace Uno.GameClient.Wpf.Services
{
    internal static class ImageServices
    {
        private static ImageSourceConverter imageSourceConverter = new();

        public static ImageSource? RetrieveImageSourceFromUri(string uri)
        {
            return imageSourceConverter.ConvertFrom(new Uri(uri)) as ImageSource;
        }
    }
}
