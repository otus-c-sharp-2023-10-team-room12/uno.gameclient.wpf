﻿using Microsoft.AspNetCore.SignalR.Client;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.Rooms;

namespace Uno.GameClient.Wpf.Services
{
    internal interface INavigationService
    {
        public HubConnection? SignalrHubConnection { get; set; }
        void ShowLobbyWindow(UserData? user);
        void ShowAuthWindow();
        void ShowGameBoardWindow(RoomData roomData, UserData user);
        void CloseCurrentWindow();
        void UpdateMutex(string uniqueMutexName);
        bool CheckForMutex();
        void CloseMutex();
        void ShowErrorMessage(string errorMessage);
    }
}
