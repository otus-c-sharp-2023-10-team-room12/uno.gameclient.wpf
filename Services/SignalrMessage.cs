﻿namespace Uno.GameClient.Wpf.Services
{
    public static class SignalrMessage
    {
        public const string ChangeGameState = "ChangeGameState";
        public const string CreateRoom = "CreateRoom";
        public const string DisconnectFromRoom = "DisconnectFromRoom";
        public const string GenerateDeck = "GenerateDeck";
        public const string GetDeckCard = "GetDeckCard";
        public const string GetPlayerCards = "GetPlayerCards";
        public const string GetRooms = "GetRooms";
        public const string GetRoomPlayers = "GetRoomPlayers";
        public const string JoinToRoom = "JoinToRoom";
        public const string PlayCard = "PlayCard";
    }
}
