﻿using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Uno.GameClient.Wpf.Services
{
    public class UrlStringToImageSourceConverter : IValueConverter
    {
        private readonly Dictionary<string, ImageSource?> imageSources = new();

        public ImageSource? GetImageSource(string imageUrl)
        {
            if (!imageSources.ContainsKey(imageUrl))
                imageSources[imageUrl] = ImageServices.RetrieveImageSourceFromUri(imageUrl);
            return imageSources[imageUrl];
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ImageSource? imageSource = null;
            if (value is string urlString && !string.IsNullOrEmpty(urlString))
                imageSource = GetImageSource(urlString);
            return imageSource ?? DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
