﻿using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using System.Windows;
using Uno.CommonLibrary.Auth;
using Uno.CommonLibrary.Common;
using Uno.CommonLibrary.Rooms;
using Uno.GameClient.Wpf.ViewModels;
using Uno.GameClient.Wpf.Views;

namespace Uno.GameClient.Wpf.Services
{
    internal class NavigationService : INavigationService
    {
        public static Mutex? mutex;

        public void UpdateMutex(string uniqueMutexName)
        {
            mutex = new Mutex(false, uniqueMutexName);
        }

        public void CloseMutex()
        {
            if (mutex != null)
            {
                mutex.Close();
                mutex = null;
            }
        }

        private Window? currentWindow,
                        previousWindow;

        public HubConnection? SignalrHubConnection { get; set; }

        public void CloseCurrentWindow()
        {
            currentWindow?.Close();
        }

        private void ShowNewWindow(Window window, bool toClosePreviousWindow, BaseViewModel viewModel)
        {
            if (window != null)
            {
                previousWindow = currentWindow;
                currentWindow = window;
                if (viewModel != null)
                {
                    currentWindow.DataContext = viewModel;
                }
                currentWindow.Show();
                if (toClosePreviousWindow)
                    previousWindow?.Close();
            }
        }

        public async void ShowLobbyWindow(UserData? user)
        {
            // создание подключения
            SignalrHubConnection = new HubConnectionBuilder()
                .WithUrl(ServicesDictionary.GetServiceAddress("SignalR", false) + "/gamehub", options => 
                {
                    options.AccessTokenProvider = () => Task.FromResult(user!.Token);
                    options.SkipNegotiation = true;
                    options.Transports = HttpTransportType.WebSockets;
                })
                .ConfigureLogging(logging => {
                    logging.SetMinimumLevel(LogLevel.Information);
                })
                //.WithAutomaticReconnect()
                .Build();
            var viewModel = new LobbyWindowViewModel(this, user);
            await viewModel.Connect();
            ShowNewWindow(new LobbyWindow(), true, viewModel);
        }

        public async void ShowGameBoardWindow(RoomData room, UserData user)
        {
            var viewModel = new GameBoardViewModel(this, room, user);
            await viewModel.Connect();
            ShowNewWindow(new GameBoardWindow(), true, viewModel);
        }

        public void ShowAuthWindow()
        {
            ShowNewWindow(new AuthWindow(), false, new AuthWindowViewModel(this));
        }

        public bool CheckForMutex()
        {
            if (mutex != null)
            {
                try
                {
                    if (mutex.WaitOne(0, false))
                    {
                        return true;
                    }
                }
                catch (Exception e)
                {
                    ShowErrorMessage(e.ToString());
                }
            }
            return false;
        }

        public void ShowErrorMessage(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
                MessageBox.Show(currentWindow, errorMessage, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
