﻿using Uno.GameClient.Wpf.Models;
using Uno.GameClient.Wpf.ViewModels;

namespace Uno.GameClient.Wpf.Services
{
    internal class CardGraphicModelsFactory
    {
        private readonly Dictionary<KeyValuePair<int, CardColour>, CardGraphicModelFront> cardGraphicModelsFront = new();
        private CardGraphicModelBack? cardGraphicModelsBack;

        /// <summary>
        /// Получить "рубашку" карты
        /// </summary>
        /// <returns></returns>
        public CardGraphicModelBack GetCardGraphicModelBack()
        {
            cardGraphicModelsBack ??= new CardGraphicModelBack();
            return cardGraphicModelsBack;
        }

        /// <summary>
        /// Получить "лицевую" сторону карты
        /// </summary>
        /// <param name="value">Значение карты</param>
        /// <param name="colour">Цвет карты</param>
        /// <returns></returns>
        public CardGraphicModelFront GetCardGraphicModelFront(int value, CardColour colour)
        {
            var key  = new KeyValuePair<int, CardColour> (value, colour);
            if (!cardGraphicModelsFront.ContainsKey(key))
                cardGraphicModelsFront[key] = new CardGraphicModelFront(value, colour);
            return cardGraphicModelsFront[key];
        }
    }
}
