﻿namespace Uno.GameClient.Wpf.Models
{
    /// <summary>
    /// Состояние игры
    /// </summary>
    enum GameState
    {
        /// <summary>
        /// Неопределённое
        /// </summary>
        Undefined,
        /// <summary>
        /// В ожидании подключения игроков
        /// </summary>
        WaitingForAllPlayersConnection,
        /// <summary>
        /// В ожидании начала игры
        /// </summary>
        WaitingForStarting,
        /// <summary>
        /// В ожидании хода активного игрока
        /// </summary>
        WaitingForCurrentPlayerAction,
        /// <summary>
        /// Игра начата
        /// </summary>
        Started,
        /// <summary>
        /// Игра закончена
        /// </summary>
        Ended
    }

}
