﻿namespace Uno.GameClient.Wpf.Models
{
    internal static class Extensions
    {
        internal static CardColour GetCardColourFromGamePlayColourNumber(this int Color)
        {
            return Color switch
            {
                0 => CardColour.Red,
                1 => CardColour.Blue,
                2 => CardColour.Green,
                3 => CardColour.Yellow,
                _ => CardColour.Undefined,
            };
        }

    }
}
