﻿namespace Uno.GameClient.Wpf.Models
{
    /// <summary>
    /// Цвет карты
    /// </summary>
    enum CardColour
    {
        /// <summary>
        /// Красный цвет
        /// </summary>
        Red = 0,
        /// <summary>
        /// Желтый цвет
        /// </summary>
        Yellow,
        /// <summary>
        /// Зеленый цвет
        /// </summary>
        Green,
        /// <summary>
        /// Синий цвет
        /// </summary>
        Blue,
        /// <summary>
        /// Цвет для специальных карт действий
        /// </summary>
        Special,
        /// <summary>
        /// Неопределённый цвет
        /// </summary>
        Undefined
    }
}
